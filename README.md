# i3wm Helper Program

This program aids my needs when using the [i3 window manager](https://i3wm.org/)
(actually i3-gaps).  The functions include:

 * Act as `smart_gaps` alternative.

   Should you insist on knowing the reason I did this instead of simply turning
   on `smart_gaps` - the built-in `smart_gaps` option disables
   the effect of `gaps top /* some value */`, which is needed when using
   [polybar](https://github.com/polybar/polybar) with `override-redirect = true`
   option turned on, which in turn is needed to fix the problem where windows
   cast shadows on polybar itself but not the system tray when used together
   with the [picom](https://github.com/yshui/picom) compositor.

 * Print focused window's name (second string in `WM_CLASS`) to STDOUT.

   This is used in my own polybar custom script module to show the app's name
   on the bar.  The module is like this:

   ```dosini
   [module/title]
   type = custom/script
   exec = i3-smart-gap
   tail = true
   interval = 0
   ```
