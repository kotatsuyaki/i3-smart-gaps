#[macro_use]
extern crate log;

use std::{env::args, process::Command};

use i3ipc::{event::Event, I3Connection, I3EventListener, Subscription};
use regex::Regex;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    let args = args().into_iter().collect::<Vec<_>>();
    if args.get(1).map(String::as_str) == Some("-v") {
        let version: Option<&'static str> = option_env!("CI_COMMIT_SHORT_SHA");
        println!("i3-smart-gap {}", version.unwrap_or("unknown"));
        return Ok(());
    }

    // Gaps helper has to be explicitly enabled using `-g` option.
    let mut listener = Listener::new(args.get(1).map(String::as_str) == Some("-g"))?;
    listener.listen()?;

    Ok(())
}

struct Listener {
    connection: I3Connection,
    listener: I3EventListener,
    window_class_regex: Regex,
    enable_gaps_helper: bool,
}

impl Listener {
    fn new(enable_gaps_helper: bool) -> Result<Self, Box<dyn std::error::Error>> {
        let mut connection = I3Connection::connect().map_err(Error::i3error)?;
        info!(
            "{}",
            connection
                .get_version()
                .map_err(Error::i3error)?
                .human_readable
        );

        let mut listener = I3EventListener::connect().map_err(Error::i3error)?;
        let subs = [
            Subscription::Window,
            Subscription::Mode,
            Subscription::Output,
            Subscription::Binding,
            Subscription::Workspace,
            Subscription::BarConfig,
        ];
        listener.subscribe(&subs).map_err(Error::i3error)?;

        Ok(Self {
            connection,
            listener,
            window_class_regex: Regex::new(r#""(.*?)""#)?,
            enable_gaps_helper,
        })
    }

    fn listen(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let Listener {
            listener,
            connection,
            window_class_regex,
            enable_gaps_helper,
        } = self;

        for event in listener.listen() {
            match event.map_err(Error::i3error)? {
                Event::WindowEvent(_) | Event::WorkspaceEvent(_) => {
                    Self::handle_window_event(connection, window_class_regex, *enable_gaps_helper)?;
                }
                _ => {}
            }
        }

        Ok(())
    }

    fn handle_window_event(
        connection: &mut I3Connection,
        window_class_regex: &Regex,
        enable_gaps_helper: bool,
    ) -> Result<(), Box<dyn std::error::Error>> {
        if enable_gaps_helper {
            let maybe_focused_ws_name = connection
                .get_workspaces()?
                .workspaces
                .into_iter()
                .find(|ws| ws.focused)
                .map(|ws| ws.name);

            let tree = connection.get_tree()?;
            let workspaces = find_workspaces(&tree);
            let maybe_focused_ws_node = workspaces
                .iter()
                .find(|ws| ws.name == maybe_focused_ws_name);

            // Enable inner gaps only if there's more than one child.
            if let Some(focused_ws_node) = maybe_focused_ws_node {
                let gaps_cmd = if focused_ws_node.nodes.len() <= 1 {
                    "gaps inner current set 0"
                } else {
                    "gaps inner current set 5"
                };
                connection.run_command(gaps_cmd)?;
            }
        }

        // Adapted from this shell command:
        // `xprop -id $(xdotool getactivewindow) WM_CLASS`
        let active_window = String::from_utf8(
            Command::new("xdotool")
                .args(&["getactivewindow"])
                .output()?
                .stdout,
        )?;
        let wm_class_output = String::from_utf8(
            Command::new("xprop")
                .args(&["-id", &active_window, "WM_CLASS"])
                .output()?
                .stdout,
        )?;

        // Try to take the text in the second pair of quotations.
        // The output is like `WM_CLASS(STRING) = "Alacritty", "Alacritty"`.
        if let Some(cap) = window_class_regex
            .captures_iter(&wm_class_output)
            // Skip first (i.e. get second match)
            .skip(1)
            .next()
        {
            info!("window class: {}", &cap[1]);

            let text_to_print = match &cap[1] {
                // Chromium on gentoo returns unreasonably long name; replace that
                "Chromium-bin-browser-chromium" => "Chromium",
                _ => &cap[1],
            };
            println!("{}", text_to_print);
        } else {
            warn!("Can't get window class");

            // Print newline to flush
            println!("");
        }

        Ok(())
    }
}

use error::Error;
mod error {
    #[derive(Debug)]
    pub enum Error {
        I3Error(String),
    }

    impl Error {
        pub fn i3error<E: std::error::Error>(inner: E) -> Self {
            Error::I3Error(format!("{}", inner))
        }
    }

    impl std::fmt::Display for Error {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    impl std::error::Error for Error {}
}

fn find_workspaces(root_node: &i3ipc::reply::Node) -> Vec<i3ipc::reply::Node> {
    let mut workspace_nodes = vec![];

    // Test if it should be included as workspace node
    let is_workspace_nodetype = matches!(root_node.nodetype, i3ipc::reply::NodeType::Workspace);
    let is_not_scratch = root_node.name.as_ref().map(String::as_str) != Some("__i3_scratch");
    if is_workspace_nodetype && is_not_scratch {
        workspace_nodes.push(root_node.clone());
    }

    // Recursively search
    for child_node in root_node.nodes.iter() {
        workspace_nodes.extend(find_workspaces(child_node));
    }

    workspace_nodes
}
